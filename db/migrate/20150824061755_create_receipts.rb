class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
      t.references :store, index: true
      t.string :customer_id
      t.decimal :total_amount, :precision => 9, :scale => 2
      t.integer :total_items

      t.timestamps
    end
  end
end
