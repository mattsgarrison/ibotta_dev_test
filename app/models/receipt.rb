class Receipt < ActiveRecord::Base
  belongs_to :customer, foreign_key: :customer_id

  validates :total_amount, numericality: true
  validates :total_items,  numericality: true
end
