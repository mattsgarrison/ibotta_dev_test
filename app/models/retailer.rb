class Retailer < ActiveRecord::Base
  has_many :stores
  has_many :events, through: :stores
end
