class Api::V1::StoresController < Api::V1::BaseController

  before_filter :set_store

  private

  def set_store
    @store =  Store.where(id: params[:store_id].to_i).first
  end

end
