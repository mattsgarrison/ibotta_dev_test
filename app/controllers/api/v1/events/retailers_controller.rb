class Api::V1::Events::RetailersController < ::Api::V1::BaseController


  def show
    @retailer = Retailer.where(id: retailer_id).first
    if date_param.present?
      @events = @retailer.events.where("event_at >= ? AND event_at <= ?",
        date_param.to_time, date_param.to_time.end_of_day
      )
    else
      @events = @retailer.events
    end
  end

  private

  def retailer_id
    params[:id].to_i
  end

  def date_param
    if params[:date]
      params[:date].to_date
    else
      nil
    end
  end

end
