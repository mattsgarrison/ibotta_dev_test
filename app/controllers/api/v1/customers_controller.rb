class Api::V1::CustomersController < ::Api::V1::StoresController

  before_filter :set_customer


  # POST
  def receipt
    if @store.present? && @customer.present?
      @receipt = @customer.receipts.new(store_id: @store.id,
        total_items: receipt_params['total_items'],
        total_amount: receipt_params['total_amount']
      )

      if @receipt.save
        render json: { success: true, receipt_id: @receipt.id }, status: :created
      else
        render json: { success: false, receipt_id: nil }, status: :unprocessable_entity
      end

    else
      render json: { success: false, receipt_id: nil }, status: :unprocessable_entity
    end
  end

  private

  def receipt_params
    params.require(:receipt).permit(:total_amount, :total_items)
  end

  def set_customer
    @customer = Customer.where(id: params[:id]).first
  end

end
