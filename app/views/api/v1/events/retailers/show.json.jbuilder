json.events @events do |event|
  json.event_id event.id
  json.lat event.lat
  json.long event.long
  json.event_at event.event_at
end
