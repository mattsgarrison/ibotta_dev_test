Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :events, only: [:index] do

      end
      namespace :events do
        resources :retailers, only: [:show] do
        end
      end

      resources :stores, only: [] do
        resources :customers, only: [] do
          member do
            post :receipt
          end
        end
      end
    end
  end
end
