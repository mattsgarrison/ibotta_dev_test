require 'spec_helper'

module Api
  module V1
    module Events
      describe RetailersController do

        render_views

        describe "GET #show" do
          let(:retailer) { create(:retailer) }
          let(:store)    { create(:store, retailer: retailer) }
          before do
            @events = (1..5).collect do
              create(:event, store: store)
            end
          end

          it "accepts a retailer ID as part of a URL path" do
            get :show, id: retailer.id, format: :json
            expect(response).to be_success
          end

          it "returns all events for that retailer id" do
            get :show, id: retailer.id, format: :json
            expect(response).to be_success
            body = JSON.parse(response.body)
            expect(body['events'].size).to eq(@events.size)
          end

          it "accepts a retailer ID as part of a URL and a data URL param" do
            get :show, id: retailer.id, date: Date.today, format: :json
            expect(response).to be_success
          end

          it "returns all of today's events for that retailer id" do
            first_event = @events[0]
            second_event = @events[1]

            first_event.update_attributes(event_at: Time.now)
            second_event.update_attributes(event_at: Time.now)

            get :show, id: retailer.id, date: Date.today, format: :json
            expect(response).to be_success
            body = JSON.parse(response.body)
            event_ids = body['events'].map{|e| e['event_id']}

            expect(event_ids).to include(first_event.id)
            expect(event_ids).to include(second_event.id)
            expect(event_ids).to_not include(@events[3])
            expect(event_ids).to_not include(@events[4])
            expect(event_ids).to_not include(@events[5])
          end

        end
      end
    end
  end
end
