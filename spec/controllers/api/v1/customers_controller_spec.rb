require 'spec_helper'

module Api
  module V1
    describe CustomersController do
      render_views

      describe "POST #receipt" do
        let(:retailer) { create(:retailer) }
        let(:store)    { create(:store, retailer: retailer) }
        let(:customer) { create(:customer) }
        before do
          @request_headers = {
            "Accept" => "application/json",
            "Content-Type" => "application/json"
          }
        end

        it "accepts all of the required parameters for a receipt" do
          post_params = {
            store_id: store.id,
            id: customer.id,
            receipt: {total_amount: 123.34, total_items: 12}
          }

          post :receipt, post_params, @request_headers
          expect(response.status).to eq(201) # created
          expect(response.content_type).to eq('application/json')
          body = JSON.parse(response.body)
          expect(body['success']).to eq(true)
          expect(body['receipt_id']).to eq(1)
        end

        it "fails if the totals are not coercable into numerics" do
          post_params = {
            store_id: store.id,
            id: customer.id,
            receipt: {total_amount: "one hundred" , total_items: "three"}
          }
          post :receipt, post_params, @request_headers

          expect(response.status).to eq(422) # created
          body = JSON.parse(response.body)
          expect(body['success']).to eq(false)
          expect(body['receipt_id']).to eq(nil)
        end

        it "fails if the customer doesn't exist" do
          post_params = {
            store_id: store.id,
            id: (customer.id + 1),
            receipt: {total_amount: 123.34, total_items: 12}
          }
          post :receipt, post_params, @request_headers

          expect(response.status).to eq(422) # created
          body = JSON.parse(response.body)
          expect(body['success']).to eq(false)
          expect(body['receipt_id']).to eq(nil)
        end

        it "fails if the store doesn't exist" do
          post_params = {
            store_id: (store.id + 1),
            id: customer.id,
            receipt: {total_amount: 123.34, total_items: 12}
          }
          post :receipt, post_params, @request_headers

          expect(response.status).to eq(422) # created
          body = JSON.parse(response.body)
          expect(body['success']).to eq(false)
          expect(body['receipt_id']).to eq(nil)
        end
      end
    end
  end
end
